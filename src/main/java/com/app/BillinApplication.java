package com.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class BillinApplication implements CommandLineRunner{
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(BillinApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		String password = "12345";
		String passwordAdmin = "admin";
		
		System.out.println("Password: " + this.bCryptPasswordEncoder.encode(password));
		System.out.println("Password Admin: " + this.bCryptPasswordEncoder.encode(passwordAdmin));
	}
}
