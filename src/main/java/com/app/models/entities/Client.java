package com.app.models.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="clients")
public class Client implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotEmpty(message="no puede estar vacío")
	@Size(min=4, max=20, message="el tamaño tiene que estar entre 4 y 20")
	private String name;
	
	@NotEmpty(message="no puede estar vacío")
	@Size(min=4, max=20, message="el tamaño tiene que estar entre 4 y 20")
	@Column(name="last_name")	
	private String lastName;
	
	@NotEmpty(message="no puede estar vacío")
	@Email(message="no es una dirección de correo bien formada")
	@Size(min=4, max=50, message="el tamaño tiene que estar entre 4 y 50")
	private String email;

	@Column(name="create_at")
	@Temporal(TemporalType.DATE)
	private Date createAt;

	private String photo;
	
	@NotNull(message="La región es obligatoria")
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_region")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Region region;

	@PrePersist
	public void prePersist() {
		this.createAt = new Date();
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getCreateAt() {
		return createAt;
	}
	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}
    
}
