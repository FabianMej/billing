package com.app.models.services;

import java.util.List;

import com.app.models.entities.Client;
import com.app.models.entities.Region;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ClientService {
	
	List<Client> findAll();

	Page<Client> findAll(Pageable pageable);
	
	Client save(Client client);
	
	Client findById(long id);
	
	void delete(long id);
	
	List<Region> findAllRegions();

}
