package com.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.app.models.entities.User;

public interface UserDao extends CrudRepository<User, Long>{
	
	User findByUsername(String username);

}
