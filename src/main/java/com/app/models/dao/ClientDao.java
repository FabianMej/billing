package com.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.models.entities.Client;
import com.app.models.entities.Region;

public interface ClientDao extends JpaRepository<Client, Long> {

	@Query("from Region")
	public List<Region> findAllRegions();
	
}
