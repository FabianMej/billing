package com.app.models.servicesImpl;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.models.dao.ClientDao;
import com.app.models.entities.Client;
import com.app.models.entities.Region;
import com.app.models.services.ClientService;

@Service
public class ClientServiceImpl implements ClientService{

	private ClientDao clientDao;

	public ClientServiceImpl(ClientDao clientDao) {
	    this.clientDao = clientDao;
    }

	@Override
	@Transactional(readOnly=true)
	public List<Client> findAll() {
		return (List<Client>) this.clientDao.findAll();
	}

    @Override
    public Page<Client> findAll(Pageable pageable) {
        return this.clientDao.findAll(pageable);
    }

    @Override
	public Client save(Client client) {
		return this.clientDao.save(client);
	}

	@Override
	public Client findById(long id) {
		return this.clientDao.findById(id).orElse(null);
	}

	@Override
	public void delete(long id) {
		this.clientDao.deleteById(id);
		
	}

	@Override
	@Transactional(readOnly=true)
	public List<Region> findAllRegions() {
		return this.clientDao.findAllRegions();
	}

}
