package com.app.models.servicesImpl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.app.models.services.UploadFileService;

@Service
public class UploadFileServiceImpl implements UploadFileService {

//	private final Logger logger = LoggerFactory.getLogger(UploadFileServiceImpl.class);
	
	private static final String UPLOAD_DIRECTORY = "uploads";
	
	@Override
	public Resource upload(String fileName) throws MalformedURLException {
		Path pathFile = this.getPath(fileName);
        Resource resource = null;

        resource = new UrlResource(pathFile.toUri());

        if (!resource.exists() || !resource.isReadable()) {
           pathFile = Paths.get("src/main/resources/static/images").resolve("no_user.svg").toAbsolutePath();
           resource = new UrlResource(pathFile.toUri());
        }
        
        return resource;
	}

	@Override
	public String copy(MultipartFile file) throws IOException {
		String nameFile = UUID.randomUUID().toString() + "_" + file.getOriginalFilename() ;
        Path path = this.getPath(nameFile);
        
        Files.copy(file.getInputStream(), path);
        
		return nameFile;
	}

	@Override
	public boolean delete(String fileName) {
		if (fileName != null && fileName.length() > 0) {
            Path pathBeforePhoto = Paths.get("uploads").resolve(fileName).toAbsolutePath();
            File fileBeforePhoto = pathBeforePhoto.toFile();
            if (fileBeforePhoto.exists() && fileBeforePhoto.canRead()) {
                fileBeforePhoto.delete();
                return true;
            }
        }
		return false;
	}

	@Override
	public Path getPath(String filename) {
		return Paths.get(UPLOAD_DIRECTORY).resolve(filename).toAbsolutePath();
	}
	

}
