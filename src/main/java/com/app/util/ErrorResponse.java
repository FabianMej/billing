package com.app.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;

public class ErrorResponse {

	public ResponseEntity<Map<String, Object>> responseErrorMessage(String message, HttpStatus status) {
		Map<String, Object> response = new HashMap<>();
		response.put("message", message);
		
		return new ResponseEntity<Map<String,Object>>(response, status);
	}
	
	public ResponseEntity<Map<String, Object>> responseErrorMessage(String message, HttpStatus status, Exception exception) {
		Map<String, Object> response = new HashMap<>();
		response.put("message", message);
		response.put("cause", exception.getClass().getCanonicalName());
		response.put("error", exception.getMessage());
		
		return new ResponseEntity<Map<String,Object>>(response, status);
	}
	
	public ResponseEntity<Map<String, Object>> responseErrorMessage(List<FieldError> errorList, HttpStatus status) {
		Map<String, Object> response = new HashMap<>();
		
		List<String> errors = errorList
				.stream()
				.map( e -> "Campo '" + e.getField() + "':  " + e.getDefaultMessage() )
				.collect(Collectors.toList());
		
		response.put("message", errors);
		
		return new ResponseEntity<Map<String,Object>>(response, status);
	}
	
}
