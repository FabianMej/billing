package com.app.controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.app.models.entities.Client;
import com.app.models.entities.Region;
import com.app.models.services.ClientService;
import com.app.models.services.UploadFileService;
import com.app.util.ErrorResponse;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/clients")
@CrossOrigin(origins = {"*"})
public class ClientRestController {

	private final Logger logger = LoggerFactory.getLogger(ClientRestController.class);
	private ClientService clientService;
	private UploadFileService uploadFileService;

	public ClientRestController(ClientService clientService, UploadFileService uploadFileService) {
	    this.clientService = clientService;
	    this.uploadFileService = uploadFileService;
    }
	
	@GetMapping("")
	public List<Client> findAll() {
		return this.clientService.findAll();
	}

	@GetMapping("/page/{page}")
	public Page<Client> findAllClients(@PathVariable int page) {
		return this.clientService.findAll(PageRequest.of(page, 10));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable long id) {

		Client client = null;
		try {
			client = this.clientService.findById(id);
			if(client == null) {
				return new ErrorResponse().responseErrorMessage("No se encontró ningún usuario para el ID " + id, HttpStatus.NOT_FOUND);
			}
			
			return new ResponseEntity<Client>(client, HttpStatus.OK);
		} catch (Exception e) {
			return new ErrorResponse().responseErrorMessage("Ocurrió un error al realizar la consulta", 
					HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
	}
	
	@PostMapping()
	@ResponseStatus(code=HttpStatus.CREATED)
	public ResponseEntity<?> save(@Valid @RequestBody Client client, BindingResult bindingResult) {
		
		if( bindingResult.hasErrors() ) {
			return new ErrorResponse().responseErrorMessage(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
		}
		
		try {
			Client newClient = this.clientService.save(client);
			
			return new ResponseEntity<Client>(newClient, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ErrorResponse().responseErrorMessage("Ocurrió un error al guardar el cliente",
					HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(code=HttpStatus.CREATED)
	public ResponseEntity<?> update(@PathVariable long id,@Valid @RequestBody Client client, BindingResult bindingResult) {
		
		if( bindingResult.hasErrors() ) {
			return new ErrorResponse().responseErrorMessage(bindingResult.getFieldErrors(), HttpStatus.BAD_REQUEST);
		}
		
		
		Client clientToUpdate = this.clientService.findById(id);
		
		if (clientToUpdate == null) {
			return new ErrorResponse().responseErrorMessage("No se encontró ningún usuario para el ID " + id, HttpStatus.NOT_FOUND);
		}
		
		clientToUpdate.setName(client.getName());
		clientToUpdate.setEmail(client.getEmail());
		clientToUpdate.setLastName(client.getLastName());
		clientToUpdate.setRegion(client.getRegion());
		
		return this.save(clientToUpdate, bindingResult);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(code=HttpStatus.NO_CONTENT)
	public ResponseEntity<?> delete(@PathVariable long id) {
		try {
		    Client client = this.clientService.findById(id);
		    this.uploadFileService.delete(client.getPhoto());

			this.clientService.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			this.logger.error(e.getMessage());
			return new ErrorResponse().responseErrorMessage("Ocurrió un error al eliminar el cliente", 
					HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
	}

	@PostMapping("/upload")
	public ResponseEntity<?> uploadImage(@RequestParam MultipartFile file, @RequestParam long id) {
        Map<String, Object> response = new HashMap<>();
        Client client = this.clientService.findById(id);
        String nameFile = "";
        
        if (!file.isEmpty()) {
            if (!isSizeImageValid(file)) {
                return new ErrorResponse().responseErrorMessage("El tamaño del archivo es mayor a 10MB",
                        HttpStatus.BAD_REQUEST);
            }


            try {
            	this.uploadFileService.delete(client.getPhoto());
                nameFile = this.uploadFileService.copy(file);
            }
            catch (IOException e) {
				this.logger.error(e.getMessage());
                return new ErrorResponse().responseErrorMessage("Ocurrió un error al cargar la imagen ",
                        HttpStatus.INTERNAL_SERVER_ERROR);
            }

            client.setPhoto(nameFile);
            this.clientService.save(client);
            response.put("cliente", client);
            response.put("mensaje", "Ha subido correctamente la imagen: " + nameFile);
        }

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/upload/{nameFile:.+}")
    public ResponseEntity<?> getPhoto(@PathVariable String nameFile) {
        Resource resource = null;
        
        try {
			resource = this.uploadFileService.upload(nameFile);
		} catch (MalformedURLException e) {
			this.logger.error(e.getMessage());
			return new ErrorResponse().responseErrorMessage("Ocurrió un error al tratar de obtener la imagen",
                  HttpStatus.INTERNAL_SERVER_ERROR);
		}

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"");
        return new ResponseEntity<>(resource, headers, HttpStatus.OK);
    }
    
    @GetMapping("/regions")
    public List<Region> findAllRegions() {
    	return this.clientService.findAllRegions();
    }


    private boolean isSizeImageValid(MultipartFile file) {
	    boolean isValid = false;
        int tenMb = 10000000;

        if (file.getSize() <= tenMb)
            isValid = true;

	    return isValid;
    }
	
}
